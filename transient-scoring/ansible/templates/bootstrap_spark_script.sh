#!/bin/bash
# create directories
S3_BUCKET={{s3_artifacts_bucket}}
S3_BUCKET_PATH={{s3_artifacts_path}}

# download config files
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/tesseract.properties /mnt/riskdna/conf/tesseract.properties
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/kafka_client_jaas.conf /mnt/riskdna/conf/kafka_client_jaas.conf
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/kafka.service.keytab /mnt/riskdna/conf/kafka.service.keytab
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/riskengine.properties /mnt/riskdna/conf/riskengine.properties
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/log4j.properties /mnt/riskdna/conf/log4j.properties
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/riskdna-engine-spark.jar /mnt/riskdna/riskdna-engine-spark.jar

#download riskcanvas keytab
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/riskcanvas.keytab /etc/riskcanvas.keytab

#instsall root certificate
java_home_path=$(dirname $(dirname $(readlink -f $(which javac))))
sudo aws s3 cp s3://$S3_BUCKET/$S3_BUCKET_PATH/rc-root-ca.pem /tmp/rc-root-ca.pem
sudo keytool -importcert -file /tmp/rc-root-ca.pem -keystore $java_home_path/jre/lib/security/cacerts -alias rc-root -storepass changeit -noprompt >/dev/null
sudo adduser riskcanvas
echo "Bootstrapping complete"