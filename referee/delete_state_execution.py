import json
import logging
import urllib3
import os
import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

session = boto3.Session(region_name='us-east-1')
client = session.client('stepfunctions')
paginator = client.get_paginator('list_executions')


def lambda_handler(event, context):
    logger.info("Event: %s", event)

    response_iterator = paginator.paginate(
        stateMachineArn='arn:aws:states:us-east-1:157161670134:stateMachine:CreateItemFromSQSStateMachine-clscr-uat',
        statusFilter='RUNNING',
        PaginationConfig={
            'PageSize': 100
        }
    )

    filtered_iterator = response_iterator.search('executions[*]')
    for key_data in filtered_iterator:
        #print(key_data)
        response = client.stop_execution(executionArn=key_data['executionArn'])
        print(str(response))

if __name__ == "__main__":
    lambda_handler('', '')