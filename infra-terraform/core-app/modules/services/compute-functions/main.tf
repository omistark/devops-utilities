data "archive_file" "create_dist_pkg" {
  //  depends_on = ["null_resource.install_python_dependencies"]
  source_dir = "${path.module}/lambda_dist_pkg/"
  output_path = var.output_path
  type = "zip"
}

data "aws_iam_role" "iam_for_lambda" {
  name = "test_iam_for_lambda"
}

resource "aws_lambda_function" "test_lambda" {
  description = "Python hello world lambda."
  function_name = "test_rc_function"
  handler = "index.lambda_handler"
  runtime = "python3.8"

  role = data.aws_iam_role.iam_for_lambda.arn
  memory_size = 128
  timeout = 300

  source_code_hash = data.archive_file.create_dist_pkg.output_base64sha256
  filename = data.archive_file.create_dist_pkg.output_path

  environment {
    variables = {
      foo = "bar"
    }
  }

  tags = {
    Environment = "Regression"
  }
}