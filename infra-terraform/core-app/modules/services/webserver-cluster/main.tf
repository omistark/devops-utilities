data "aws_ami" "amazon-linux-2" {
  most_recent = true

  filter {
    name = "name"
    values = [
      "amzn2-ami-hvm-2.0.*.0-x86_64-gp2"]
    //    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = [
      "hvm"]
  }

  owners = [
    "amazon"]
  # Canonicalami-0915bcb5fa77e4892
}

data "aws_ami" "amazon-linux-1" {
  most_recent = true

  filter {
    name = "name"
    values = [
      "amzn-ami-hvm-2018.*.0-x86_64-gp2"]
  }

  filter {
    name = "virtualization-type"
    values = [
      "hvm"]
  }

  owners = [
    "amazon"]
  # Canonicalami-0915bcb5fa77e4892
}

resource "aws_launch_template" "app_tmpl_cb" {
  name_prefix = "Test"
  image_id = var.cloudbreak_ami
  instance_type = var.instance_type
  iam_instance_profile {
    name = "RiskcanvasEC2DefaultRole"
  }
  vpc_security_group_ids = [
    "sg-0f2c7d200384e293e"]
  key_name = "core-product-key"
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "test-cb"
      Environment = "regression"
    }
  }
}

resource "aws_launch_template" "app_tmpl_2" {
  name_prefix = "Test"
  image_id = data.aws_ami.amazon-linux-2.id
  instance_type = var.instance_type
  iam_instance_profile {
    name = "RiskcanvasEC2DefaultRole"
  }
  vpc_security_group_ids = [
    "sg-0f2c7d200384e293e"]
  key_name = "core-product-key"
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "test-amzn2"
      Environment = "regression"
    }
  }
}

resource "aws_launch_template" "app_tmpl_1" {
  name_prefix = "Test"
  image_id = data.aws_ami.amazon-linux-1.id
  instance_type = var.instance_type
  iam_instance_profile {
    name = "RiskcanvasEC2DefaultRole"
  }
  vpc_security_group_ids = [
    "sg-0f2c7d200384e293e"]
  key_name = "core-product-key"
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "test-amzn1"
      Environment = "regression"
    }
  }
}

resource "aws_autoscaling_group" "app-clb" {
  max_size = 3
  min_size = 1
  desired_capacity = 1
  force_delete = true
  vpc_zone_identifier = var.vpc_subnets

  launch_template {
    id = aws_launch_template.app_tmpl_cb.id
    //    version = "$Latest"
  }

  //  initial_lifecycle_hook {
  //    name                 = "foobar"
  //    default_result       = "CONTINUE"
  //    heartbeat_timeout    = 2000
  //    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  //
  //    notification_metadata = <<EOF
  //{
  //  "foo": "bar"
  //}
  //EOF
  //
  ////    notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
  ////    role_arn                = "arn:aws:iam::729587188256:role/RiskcanvasEC2DefaultRole"
  //  }

  tag {
    key = "Environment"
    value = "regression"
    propagate_at_launch = true
  }
  tag {
    key = "Role"
    value = "app_server"
    propagate_at_launch = true
  }
  tag {
    key = "cb-user-name"
    value = "cloudbreakadmin@riskcanvas.com"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }
}

resource "aws_autoscaling_group" "app-amzn1" {
  max_size = 3
  min_size = 1
  desired_capacity = 1
  force_delete = true
  vpc_zone_identifier = var.vpc_subnets

  launch_template {
    id = aws_launch_template.app_tmpl_1.id
    //    version = "$Latest"
  }

  //  initial_lifecycle_hook {
  //    name                 = "foobar"
  //    default_result       = "CONTINUE"
  //    heartbeat_timeout    = 2000
  //    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  //
  //    notification_metadata = <<EOF
  //{
  //  "foo": "bar"
  //}
  //EOF
  //
  ////    notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
  ////    role_arn                = "arn:aws:iam::729587188256:role/RiskcanvasEC2DefaultRole"
  //  }

  tag {
    key = "Environment"
    value = "regression"
    propagate_at_launch = true
  }
  tag {
    key = "Role"
    value = "app_server"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }
}

resource "aws_autoscaling_group" "app-amzn2" {
  max_size = 3
  min_size = 1
  desired_capacity = 1
  force_delete = true
  vpc_zone_identifier = var.vpc_subnets

  launch_template {
    id = aws_launch_template.app_tmpl_2.id
    //    version = "$Latest"
  }

  tag {
    key = "Environment"
    value = "regression"
    propagate_at_launch = true
  }
  tag {
    key = "Role"
    value = "app_server"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

  //  tag {
  //    key                 = "lorem"
  //    value               = "ipsum"
  //    propagate_at_ launch = false
  //  }
}