variable "ami_id" {
  description = "ID of the AMI to provision. Default is Ubuntu 14.04 Base Image"
  default = "ami-0fc61db8544a617ed"
}

variable "instance_type" {
  description = "type of EC2 instance to provision."
  default = "t2.small"
}

variable "name" {
  description = "name to pass to Name tag"
  default = "TestServer"
}

variable "vpc_subnets" {
  description = ""
  default = ["subnet-035205d4ad6dec975"]
}

variable "cloudbreak_ami" {
  description = ""
  default = "ami-038174618db671211"
}