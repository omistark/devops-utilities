data "aws_ami" "amazon-linux-2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*.0-x86_64-gp2"]
//    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"] # Canonicalami-0915bcb5fa77e4892
}

resource "aws_instance" "admin_server" {
//  ami = var.ami_id
  ami = data.aws_ami.amazon-linux-2.id
  instance_type = var.instance_type
  availability_zone = "${var.aws_region}a"
  vpc_security_group_ids = ["sg-0f2c7d200384e293e"]
  subnet_id = "subnet-035205d4ad6dec975"
  iam_instance_profile = "RiskcanvasEC2JenkinsRole"
  key_name = "coreproduct-management-key"

  tags = {
    Name = "TestAdminServer"
    "Environment" = "regression"
    "Role" = "admin_server"
  }
}


resource "aws_ebs_volume" "admin_vol" {
  availability_zone = "${var.aws_region}a"
  size              = 10
//  encrypted = true
  type = "gp2"
//  kms_key_id = ""

  tags = {
    Name = "TestRegression"
    CloudbreakClusterName = "none"
    Customer =  "internal"
    Environment =  "regression"
//    Owner = cloudbreakadmin@riskcanvas.com
//    cb-account-name = unknown
//    cb-creation-timestamp =  1589565535
//    cb-user-name = cloudbreakadmin@riskcanvas.com
//    cb-version =  2.9.1
//    cloudbreak-ebs-encryption-677 =  cloudbreak-ebs-encryption-677
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.admin_vol.id
  instance_id = aws_instance.admin_server.id
}
