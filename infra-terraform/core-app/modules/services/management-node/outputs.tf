output "admin_instance_id" {
  value = aws_instance.admin_server.id
}

//output "app_instance_id" {
//  value = aws_instance.app_server.id
//}

output "admin_vol_id" {
  value = aws_ebs_volume.admin_vol.id
}