terraform {
  required_version = ">= 0.11.0"
  backend "s3" {
    bucket = "rc-admin-core-us-east-1"
    key    = "tfstates/identity-security"
    region = "us-east-1"
    kms_key_id = "arn:aws:kms:us-east-1:729587188256:key/4f41b698-62c3-497a-b96c-19dd22202634"
  }
  required_providers {
    archive = {
      source = "hashicorp/archive"
      version = "2.1.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  profile = "729587188256-RiskCanvasAdministrators"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = var.iam_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
    Environment = "Regression"
  }
}

resource "aws_iam_policy" "lambda_tracing" {
  name = var.iam_tracing_policy_name
  path = "/"
  description = "IAM policy for enabling tracing for a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "xray:PutTraceSegments",
        "xray:PutTelemetryRecords"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_tracing" {
  role = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_tracing.arn
}
