variable "iam_role_name" {
  description = ""
  default = "test_iam_for_lambda"
}

variable "iam_tracing_policy_name" {
  description = ""
  default = "test_lambda_tracing"
}