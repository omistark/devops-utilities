//data "aws_kms_key" "by_alias" {
//  key_id = "alias/my-key"
//}

resource "aws_s3_bucket" "test-bucket" {
  bucket = "test-rc-bucket"
  acl    = "private"

//  server_side_encryption_configuration {
//    rule {
//      apply_server_side_encryption_by_default {
//        kms_master_key_id = data.aws_kms_key.by_alias.arn
//        sse_algorithm     = "aws:kms"
//      }
//    }
//  }
  tags = {
    Name        = "test-rc-bucket"
    Environment = "Dev"
  }
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.test-bucket.arn}/*"
      ]
    }
  ]
}
POLICY
}
