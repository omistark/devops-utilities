variable "kms_key_alias" {
  description = "ID of the AMI to provision. Default is Ubuntu 14.04 Base Image"
  default = "rc_data"
}