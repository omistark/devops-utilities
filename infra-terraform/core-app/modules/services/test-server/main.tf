//data "aws_ami" "amazon-linux-2" {
//  most_recent = true
//
//  filter {
//    name   = "name"
//    values = ["amzn2-ami-hvm-2.0.*.0-x86_64-gp2"]
////    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
//  }
//
//  filter {
//    name   = "virtualization-type"
//    values = ["hvm"]
//  }
//
//  owners = ["amazon"] # Canonicalami-0915bcb5fa77e4892
//}

resource "aws_instance" "test_server" {
//  ami = var.ami_id
  ami = var.ami_id
  instance_type = var.instance_type
  availability_zone = var.az_zone
  vpc_security_group_ids = [var.sec_group]
  subnet_id = var.vpc_subnets
  iam_instance_profile = var.iam_role
  key_name = var.key_pair

  tags = {
    Name = "regression-test"
    "Environment" = "regression"
    "SecurityClassification" = "ITE"
  }
}


//resource "aws_ebs_volume" "admin_vol" {
//  availability_zone = "${var.aws_region}a"
//  size              = 10
////  encrypted = true
//  type = "gp2"
////  kms_key_id = ""
//
//  tags = {
//    Name = "TestRegression"
//    CloudbreakClusterName = "none"
//    Customer =  "internal"
//    Environment =  "regression"
//  }
//}
//
//resource "aws_volume_attachment" "ebs_att" {
//  device_name = "/dev/sdh"
//  volume_id   = aws_ebs_volume.admin_vol.id
//  instance_id = aws_instance.admin_server.id
//}
