variable "aws_region" {
  description = "AWS region"
  default = "ap-southeast-2"
}

variable "ami_id" {
  description = "ID of the AMI to provision. Default is Ubuntu 14.04 Base Image"
  default = "ami-03287435134d0b5b3"
}

variable "instance_type" {
  description = "type of EC2 instance to provision."
  default = "t2.small"
}

variable "name" {
  description = "name to pass to Name tag"
  default = "TestServer"
}

variable "vpc_subnets" {
  description = ""
  default = "subnet-0609fe88354088efb"
}

variable "key_pair" {
  default = "westpac_pre-prod"
}

variable "az_zone" {
  default = "ap-southeast-2a"
}

variable "sec_group" {
  default = "sg-05fc293b466bb9caf"
}

variable "iam_role" {
  default = "EC2DefaultRole"
}