//resource "aws_config_config_rule" "r" {
//  name = "example"
//
//  source {
//    owner             = "AWS"
//    source_identifier = "S3_BUCKET_VERSIONING_ENABLED"
//  }
//
//  depends_on = [aws_config_configuration_recorder.foo]
//}
//
//resource "aws_config_configuration_recorder" "foo" {
//  name     = "example"
//  role_arn = aws_iam_role.r.arn
//}
//
//resource "aws_iam_role" "r" {
//  name = "my-awsconfig-role"
//
//  assume_role_policy = <<POLICY
//{
//  "Version": "2012-10-17",
//  "Statement": [
//    {
//      "Action": "sts:AssumeRole",
//      "Principal": {
//        "Service": "config.amazonaws.com"
//      },
//      "Effect": "Allow",
//      "Sid": ""
//    }
//  ]
//}
//POLICY
//}
//
//resource "aws_iam_role_policy" "p" {
//  name = "my-awsconfig-policy"
//  role = aws_iam_role.r.id
//
//  policy = <<POLICY
//{
//  "Version": "2012-10-17",
//  "Statement": [
//      {
//          "Action": "config:Put*",
//          "Effect": "Allow",
//          "Resource": "*"
//
//      }
//  ]
//}
//POLICY
//}
//
//resource "aws_config_remediation_configuration" "this" {
//  config_rule_name = aws_config_config_rule.r.name
//  resource_type    = "AWS::S3::Bucket"
//  target_type      = "SSM_DOCUMENT"
//  target_id        = "AWS-EnableS3BucketEncryption"
//  target_version   = "1"
//
//  parameter {
//    name         = "AutomationAssumeRole"
//    static_value = "arn:aws:iam::875924563244:role/security_config"
//  }
//  parameter {
//    name           = "BucketName"
//    resource_value = "RESOURCE_ID"
//  }
//  parameter {
//    name         = "SSEAlgorithm"
//    static_value = "AES256"
//  }
//}