terraform {
  required_version = ">= 0.11.0"
//  backend "s3" {
//    bucket = "rc-admin-core-us-east-1"
//    key    = "tfstates/regression"
//    region = "us-east-1"
//    profile = "729587188256-RiskCanvasAdministrators"
//  }
  required_providers {
    archive = {
      source = "hashicorp/archive"
      version = "2.1.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  profile = var.aws_profile
}

provider "archive" {
  # Configuration options
}

module "activiti-db" {
  source = "./modules/activiti-db"
}
