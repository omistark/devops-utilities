resource "aws_db_instance" "default" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = "11.10"
  instance_class       = "db.t3.micro"
  name                 = "rctestdb"
  username             = "rcadmin"
  password             = "foobarbaz"
  iam_database_authentication_enabled = true
  storage_encrypted = true
  kms_key_id = "arn:aws:kms:us-east-1:729587188256:key/4f41b698-62c3-497a-b96c-19dd22202634"
  tags = {
    SecurityClassification = "ITE"
  }
  parameter_group_name = "default.postgres11"
  identifier = "rctestdb"
  skip_final_snapshot  = true
}
