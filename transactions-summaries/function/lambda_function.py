import json
import os
import boto3
import requests
import datetime
from datetime import datetime as datetime2
from datetime import date
from datetime import timedelta
# from datetime import datetime, timedelta
import csv
from dateutil.tz import *
from urllib.parse import urlparse
from botocore.exceptions import ClientError
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

s3_data_bucket = os.environ['DROP_BUCKET']
s3_report_bucket = os.environ['CSV_REPORT_BUCKET']

headers = {
    'ticket': 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLmtIMzJUejU3bGlsMFlqT3RoWGtXd2cuNERDQWdtc2o4SEFxVkx1aTdRY2tnRU1LdkVZREhqOXRSaEJOd3dXQW4xVU9JVUFTbnJPc01QN2Z2UWRDbTJLakJlbXdtLTVuQ3p5R29obF81eUxmV1o0blBPUXBKdXQ0YnJOVW1tbDl4ZFYzTjlMSmd3ckpjOGMzS3lDbmUwTlZZNmVSbnFCQVk5R2RWNlgxXzVJbTZNZ0F3eC1Cc20yNEs1UFdLZXREYXVneFpGempzQ3NuZF9GcDlvblU4UWM4SEtYU0NNdndRVkpmazUyVHZUZFpuMjU5ZVhPSVlrdjExRGYyLWtfMVc0N0lKZnV0OXFMcnh1U1NzUkNOVTk3a0FpVy1hcXpMUGpCaGEwRHNvS2F1c2xhWDVoa1h2ZURCNkxfVVlUaTdZZnZqV084OHNjdVhDZ1FJWTl3NHlqQmJabnhMYlhrZTcwMVlBQktNWmhNSGxqTzIwRUVKcDN2RjFWV29hWlRUeHB5eGMzdUFla0pkT0hrTHQ5X3ExYVNyOGNaRGxmdFJJYzF3WnF4dUYteUUwM1NoUGo5R2NBQzNrd0otYXJoNk96dHJseUx4REluSmU0bEN3bmFua1J5d2RXMmdDN1hIVlpJZm9Va0hZX1RsYWJTQ0R0cXRlMFJVSm9yLXNRSExtblJ4d3JCbTFJQ0I3NFE2TEdqbEVfNXguQzkzS1Q5YTZ4WkdTWjloOFlXMTZOZw.-KhLWJ8QYcGmY912Z7UZ7B5LeAzdbwCXQBVqu_wcUKuCfnUFuQwRfyHM7FNDAxLhqgf1ZUVMx8nv1JFKRnIIUg',
    'Content-Type': 'application/json'
}
distinct_query = {'query': 'SELECT count(distinct transactiondetailid) from "transactiondetail"',
                  'token': 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXV'}
total_query = {'query': 'SELECT COUNT (*) FROM "transactiondetail"',
               'token': 'XVCJ9.ZXlKNmFYQWlPaUpFUlVZaUpFUl'}
current_day = "01/01/2021"
print(os.environ['AWS_REGION'])

s3_path = str(datetime2.today().date())
query_day = ''

session = boto3.session.Session(region_name=os.environ['AWS_REGION'])
s3_client = session.client('s3')
athena_client = session.client('athena')
ses_client = session.client('ses')

total_transactions = 0
distinct_transactions = 0
processed_count = 0


def lambda_handler(event, context):
    global total_transactions, distinct_transactions, processed_count, query_day
    query_end_time = datetime2.strptime(event['time'], '%Y-%m-%dT%H:%M:%SZ')
    query_day = str(query_end_time.date())

    print('Query Day: ', query_day)
    d = query_end_time.date() - timedelta(days=1)
    query_day_fmt = d.strftime("%Y%m%d")
    file_valid = drop_file_check(query_day_fmt)

    if file_valid:
        # # query Athena
        get_trans_from_athena(event['time'])
        total_transactions = get_count_from_csv('total_count.csv')
        print('Athena Total Count: ' + str(total_transactions))
        distinct_transactions = get_count_from_csv('distinct_count.csv')
        print('Athena Distinct Count: ' + str(distinct_transactions))

        # # query Elasticsearch
        elastic_data = get_trans_from_elasticsearch(event['time'])
        processed_count = elastic_data['totalHits']
        print('Elastic Total Count: ' + str(processed_count))

        # # create Missing transaction CSV file
        write_summary_csv()

        # # Push file to S3 bucket
        push_to_s3()

        # # send email
        send_email_notification()

        # # send slack

        return {
            'statusCode': 200,
            'body': 'Reconciliation Complete'
        }

    else:
        return {
            'statusCode': 200,
            'body': 'No Drop File'
        }


def get_trans_from_athena(event_time):
    global distinct_query, total_query
    query_day_time = datetime.datetime.strptime(event_time, '%Y-%m-%dT%H:%M:%SZ')
    query_day = query_day_time.date()
    query_day = query_day.strftime("%Y%m%d")
    # print('Query: ' + str(query_day))
    # print('Filename: ' + 'TransactionDetail_' + query_day + '.txt')

    distinct_csv_path = run_athena_query(distinct_query)
    total_csv_path = run_athena_query(total_query)

    # time.sleep(3)

    # # pull Athena result from S3
    # print(distinct_csv_path)
    # print(total_csv_path)
    get_query_from_s3(distinct_csv_path, 'distinct_count.csv')
    get_query_from_s3(total_csv_path, 'total_count.csv')


def get_query_from_s3(athena_query_result_file, target_file):
    print('S3 Path: ' + str(athena_query_result_file))
    print('S3 Path: ' + str(athena_query_result_file.path[1:]))
    s3_client.download_file(athena_query_result_file.netloc, athena_query_result_file.path[1:], '/tmp/' + target_file)


def run_athena_query(query):
    # print(query)
    response = athena_client.start_query_execution(
        QueryString=query['query'],
        ClientRequestToken=str(query['token']),
        QueryExecutionContext={
            'Database': 'apex-raw',
            'Catalog': 'AwsDataCatalog'
        },
        ResultConfiguration={
            'OutputLocation': 's3://apex-gluejob-status-email/athena/transactiondetail/'
        })
    # print(response)
    query_resp = response
    query_execution_response = athena_client.get_query_execution(QueryExecutionId=query_resp['QueryExecutionId'])
    print('Query Execution Id: ' + str(query_execution_response['QueryExecution']['QueryExecutionId']))

    # print(query_execution_response['QueryExecution']['ResultConfiguration']['OutputLocation'])

    o = urlparse(query_execution_response['QueryExecution']['ResultConfiguration']['OutputLocation'])
    print(o.path[1:])

    return o
    # return response


def get_count_from_csv(result_file):
    total = 0
    with open('/tmp/' + result_file, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # numline = len(csvfile.readlines())
        #
        # for row in spamreader:
        #     print(', '.join(row))
        rows = list(csv_reader)
        total = rows[1][0]
        total = total.strip("\"")
    return total


def get_trans_from_elasticsearch(event_time):
    global current_day
    query_day_time = datetime.datetime.strptime(event_time, '%Y-%m-%dT%H:%M:%SZ')
    query_day = query_day_time.date()
    query_day = query_day.strftime("%m/%d/%Y")
    current_day = query_day
    # print(query_day)
    try:
        # elasticsearch_url = "https://apex.riskcanvas.io/eds/transactions/scan"
        elasticsearch_url = "http://10.1.3.103:8080/eds/transactions/scan"
        # payload = {"track_total_hits": "true", "size": 0, "query": {"query_string": {"query": "*"}}}
        payload = {
            "track_total_hits": "true",
            "size": 0,
            "query": {
                "bool": {
                    "should": [
                        {
                            "bool": {
                                "must": [
                                    {
                                        "range": {
                                            "transactionDate": {
                                                "gte": query_day,
                                                "lte": query_day
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        }
        response = requests.post(elasticsearch_url, data=json.dumps(payload),
                                 headers=headers, verify=False, timeout=30)

        print('## ElasticSearch Response: ' + str(response.text))
        return json.loads(response.text)
    except requests.exceptions.ConnectionError as e:
        print(e)
        return {"code": "500", "data": [], "totalHits": 0, "status": "Object successfully Retrieved"}


def write_summary_csv():
    global distinct_transactions, total_transactions, current_day
    # output file
    failed_count = int(distinct_transactions) - int(processed_count)
    print('Failed: ' + str(failed_count))
    # outputfile = 'apex_gluejob_statuscount_' + time.strftime("%b%d%Y_%H_%M_%S", t_local) + '.csv'
    outputfile = 'apex_gluejob_statuscount.csv'
    outputfilepath = '/tmp/' + outputfile
    print(outputfilepath)
    with open(outputfilepath, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Date and time", "Total Count", "Duplicate Count", "Processed Count", "Failed Count"])
        writer.writerow([current_day, total_transactions, distinct_transactions, processed_count, failed_count])


def push_to_s3():
    global s3_path, query_day
    # logger.info('## S3 Path: ' + str(s3_path))
    outputfile = 'apex_gluejob_statuscount.csv'
    outputfilepath = '/tmp/' + outputfile
    try:
        # logger.info('## Saving Requests CSV file: ' + str(s3_path + '/' + query_day + '.requests.csv'))
        response = s3_client.upload_file(outputfilepath, s3_report_bucket,
                                         'reports/' + query_day + '/reconciliation.csv')
    except ClientError as e:
        print(e)
        # logging.error(e)


def send_email_notification():
    # Email send
    CHARSET = "UTF-8"
    outputfile = 'apex_gluejob_statuscount.csv'
    outputfilepath = '/tmp/' + outputfile

    sender = 'omari.felix@genpact.com'
    recipients = ['prakasha@riskcanvas.com']
    subject = "APEX Job Execution status report on " + date.today().strftime("%d%m%Y")

    attachment = outputfilepath
    print("Attached file name : " + str(attachment))
    body_text = "Please download the attached latest APEX job execution result"

    msg = MIMEMultipart()
    # Add subject, from and to lines.
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = recipients
    textpart = MIMEText(body_text)
    msg.attach(textpart)
    reader = open(attachment, 'r')
    try:
        data = reader.read()
        att = MIMEApplication(data)
        att.add_header('Content-Disposition', 'attachment', filename=outputfile)
        msg.attach(att)
        # print(msg)

        response = ses_client.send_email(
            Source=sender,
            Destination={
                'ToAddresses': recipients,
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': CHARSET,
                        'Data': data,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            }
            # RawMessage={'Data': msg.as_string()}
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:", response['MessageId'])
    finally:
        reader.close()

    # Delete file when done, to clear space for future execution
    os.remove(outputfilepath)

    return {"status": "success", "message": "Email sent!"}


def drop_file_check(query_day_fmt):
    global s3_data_bucket
    object_key = 'TransactionDetail_' + query_day_fmt + '.txt'
    print(object_key)
    response = s3_client.head_object(Bucket=s3_data_bucket, Key=object_key)
    print(response['ResponseMetadata']['HTTPStatusCode'])
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return True
    else:
        return False


def test_handler(event, context):
    global session, xray_client, s3_client, secrets_client
    session = boto3.session.Session(region_name=os.environ['AWS_REGION'], profile_name=os.environ['AWS_PROFILE'])
    xray_client = session.client('xray')
    s3_client = session.client('s3')
    secrets_client = session.client('secretsmanager')

    return lambda_handler(event, context)


if __name__ == '__main__':
    lambda_handler({"time": "2021-03-24T14:30:00Z"}, 'y')
