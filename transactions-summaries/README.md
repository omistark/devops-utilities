# TRANSACTION SUMMARIES (RECONCILIATION) #

This is a Python script designed to be uploaded to AWS Lambda as a function
and runs queries through AWS Athena to retrieve transactions. Next, a query 
is made to an Elasticsearch endpoint to retrieve for a single date (24 hour
period). Next, it will trigger notification by email and slack. Finally, a 
CSV is created and then store in an AWS S3 bucket.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Summary of set up
* Configuration
There are two AWS Athena calls executed in the lambda function. Each require a
  token to reference specific executions. The call to the Elasticsearch server
  requires an authentication token in the header (currently hardcoded). The 
  Elasticsearch endpoint is currently hard coded.
* Dependencies
1. AWS Athena (catalog name and database name. This is reference data created from
   AWS Glue jobs)
2. AWS S3 (bucket location to put AWS Athena query results)
3. Elasticsearch (Endpoint to execute queries. This can be a DNS name or IP address)
4. AWS S3 (bucket to put the resulting CSV files)
5. AWS SES (to send email notification on count discrepancy)

* How to run tests
  
* Deployment instructions
There are two methods by which you can deploy. By running the `update.sh` script
  the lambda function will be deployed to AWS. You can then log into the AWS console
  and configure the lambda to run on a regular schedule.

Summary of set up

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact