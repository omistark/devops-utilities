#!/bin/bash
export AWS_PROFILE=791906096096-RiskCanvasAdministrators
export AWS_REGION=us-east-1
export DROP_BUCKET=apexfiledrop
export CSV_REPORT_BUCKET=apex-gluejob-status-email
export AWS_LAMBDA_FUNCTION_NAME=Apex_Transaction_Summaries


# run tests
#set -eo pipefail
#python3 function/lambda_function.test.py
#
## artifact location
#
#
##### build layer
#rm -rf package
#rm -rf my-deployment-package.zip
#cd function

##### for library layers ####
#pip install --target ../package/python -r requirements.txt
#cd ../package
#zip -r ../my-deployment-package.zip .
##cd ../../function
#
#### for function ####
zip ../my-deployment-package.zip lambda_function.py
cd ..

##### deploy function
aws --profile $AWS_PROFILE --region $AWS_REGION lambda update-function-code --function-name $AWS_LAMBDA_FUNCTION_NAME --zip-file fileb://my-deployment-package.zip
#aws --profile $AWS_PROFILE --region $AWS_REGION lambda publish-version --function-name $AWS_LAMBDA_FUNCTION_NAME

# invoke function
##FUNCTION=$(aws cloudformation describe-stack-resource --stack-name blank-python --logical-resource-id function --query 'StackResourceDetail.PhysicalResourceId' --output text)
#aws --profile $AWS_PROFILE --region $AWS_REGION --cli-connect-timeout 900 --cli-read-timeout 900 lambda invoke --function-name Sanctions-TransactionCount-Reconciliation --payload file://event.json out.json --log-type Tail --invocation-type RequestResponse
#cat out.json
#echo "Done"
#sleep 2

#cleanup
#STACK=blank-python
#if [[ $# -eq 1 ]] ; then
#    STACK=$1
#    echo "Deleting stack $STACK"
#fi
#FUNCTION=$(aws cloudformation describe-stack-resource --stack-name $STACK --logical-resource-id function --query 'StackResourceDetail.PhysicalResourceId' --output text)
#aws cloudformation delete-stack --stack-name $STACK
#echo "Deleted $STACK stack."
#if [ -f bucket-name.txt ]; then
#    ARTIFACT_BUCKET=$(cat bucket-name.txt)
#    if [[ ! $ARTIFACT_BUCKET =~ lambda-artifacts-[a-z0-9]{16} ]] ; then
#        echo "Bucket was not created by this application. Skipping."
#    else
#        while true; do
#            read -p "Delete deployment artifacts and bucket ($ARTIFACT_BUCKET)? (y/n)" response
#            case $response in
#                [Yy]* ) aws s3 rb --force s3://$ARTIFACT_BUCKET; rm bucket-name.txt; break;;
#                [Nn]* ) break;;
#                * ) echo "Response must start with y or n.";;
#            esac
#        done
#    fi
#fi
#while true; do
#    read -p "Delete function log group (/aws/lambda/$FUNCTION)? (y/n)" response
#    case $response in
#        [Yy]* ) aws logs delete-log-group --log-group-name /aws/lambda/$FUNCTION; break;;
#        [Nn]* ) break;;
#        * ) echo "Response must start with y or n.";;
#    esac
#done
#rm -f out.yml out.json function/*.pyc
#rm -rf package function/__pycache__