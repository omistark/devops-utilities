#!/bin/bash
export AWS_PROFILE=791906096096-RiskCanvasAdministrators
export DROP_BUCKET=apex-gluejob-status-email
export AWS_REGION=us-east-1

# run tests
set -eo pipefail
python3 function/lambda_function.test.py
#
## artifact location
#
#
# build layer
rm -rf package
#rm -rf my-deployment-package.zip
cd function
#
##### for library layers ####
#pip install --target ../package/python -r requirements.txt
#cd ../package
#zip -r ../my-deployment-package.zip .
##cd ../../function
#
#### for function ####
zip ../my-deployment-package.zip lambda_function.py
cd ..

# deploy function
aws --profile $AWS_PROFILE --region $AWS_REGION lambda update-function-code --function-name Sanctions-TransactionCount-Reconciliation --zip-file fileb://my-deployment-package.zip

# invoke function
##FUNCTION=$(aws cloudformation describe-stack-resource --stack-name blank-python --logical-resource-id function --query 'StackResourceDetail.PhysicalResourceId' --output text)
#aws --profile $AWS_PROFILE --region $AWS_REGION --cli-connect-timeout 900 --cli-read-timeout 900 lambda invoke --function-name Sanctions-TransactionCount-Reconciliation --payload file://event.json out.json --log-type Tail --invocation-type RequestResponse
#cat out.json
#echo "Done"
#sleep 2