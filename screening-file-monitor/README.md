# SCREENING FILE MONITOR #

This is a Python script designed to be uploaded to AWS Lambda as a function
and is event triggered. When an AWS S3 "PUT" action occurs, record of the event
is sent to an SQS queue. A lambda, loaded with this Python script, will consume
details from that event and then run an S3 call to check for that S3 object successful
presence.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
**Summary of set up**
1. Deploy the Lambda function
2. Configure an event notification to the S3 bucket [Enabling and configuring event notifications using the Amazon S3 console](https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-event-notifications.html)

**Configuration**
**Dependencies**
1. Create a S3 bucket where you expect files to be uploaded
2. Create a SQS queue that will receive the notification
3. Create a SNS topic that will notify users if a file cannot be found

* Deployment instructions
There are two methods by which you can deploy. By running the `update.sh` script
  the lambda function will be deployed to AWS. You can then log into the AWS console
  and configure the lambda to run on a regular schedule.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact