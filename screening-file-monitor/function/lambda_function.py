import json
import boto3
import os

session = boto3.session.Session(region_name=os.environ['AWS_REGION'])
sqs_client = session.client('sqs')


def lambda_handler(event, context):
    queue_url = 'https://sqs.us-east-1.amazonaws.com/157161670134/ScreeningFileDrop'

    messages = []

    while True:
        resp = sqs_client.receive_message(
            QueueUrl=queue_url,
            AttributeNames=['All'],
            MaxNumberOfMessages=10
        )

        try:
            messages.extend(resp['Messages'])
        except KeyError:
            break

        entries = [{'Id': msg['MessageId'], 'ReceiptHandle': msg['ReceiptHandle']}
                   for msg in resp['Messages']
                   ]

        # resp = sqs_client.delete_message_batch(
        #     QueueUrl=queue_url, Entries=entries
        # )

        if len(resp['Successful']) != len(entries):
            raise RuntimeError(
                f"Failed to delete messages: entries={entries!r} resp={resp!r}"
            )

    # print('Count: ' ,len(response['Messages']))
    # for message in response['Messages']:
    #     print('Message: ' + str(message['MessageId']))
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
