import json
import logging
import urllib3
import os
import boto3
import csv
import botocore
import ast

logger = logging.getLogger()
logger.setLevel(logging.INFO)

account_listing = [
    'rc-apex-RiskCanvasAdministrators',
    '434409746335-RiskCanvasAdministrators',
    '309224075787-RiskcanvasAdministrators',
    'rc-vanguard-RiskcanvasAdministrators',
    'rc-westpac-RiskcanvasAdministrators',
    'rc-tda-RiskCanvasAdministrators',
    'rc-core-RiskCanvasAdministrators',
    'rc-schwab-RiskCanvasAdministrators',
    'rc-santander-RiskCanvasAdministrators',
    'rc-rabobank-RiskCanvasAdministrators',
    'rc-e3-RiskCanvasAdministrators',
    'rc-shared-RiskCanvasAdministrators'
]
region_listing = [
    'us-east-1',
    'us-east-2',
    'us-west-1',
    'us-west-2',
    'ap-south-1',
    'ap-northeast-3',
    'ap-northeast-2',
    'ap-southeast-1',
    'ap-southeast-2',
    'ap-northeast-1',
    'ca-central-1',
    'eu-central-1',
    'eu-west-1',
    'eu-west-2',
    'eu-west-3',
    'eu-north-1',
    'sa-east-1'
]

instance_id_list = []
instance_details_list = []
not_found_list = []

outdated_list = []

valid_id_list = []

terminated_list = []

def grab_all_instances():
    # store instance ids
    for account in account_listing:
        instance_count = 0
        for region in region_listing:
            # print(f'{account} {region}')
            try:
                session = boto3.Session(profile_name=account)
                client = session.client(service_name='ec2', region_name=region)
                paginator = client.get_paginator('describe_instances')
                #
                # # response =
                for page in paginator.paginate(Filters=[{
                    'Name': 'instance-state-name',
                    'Values': [
                        'pending', 'running', 'shutting-down', 'terminated', 'stopping', 'stopped'
                    ]
                }
                ]):
                    for r in page["Reservations"]:
                        for i in r['Instances']:
                            instance_id_list.append(str(i["InstanceId"]))
                            instance_details_list.append(i)
                            instance_count += 1
            except botocore.exceptions.ClientError as error:
                print(f'Processed {account} {region} lines: {error}')

        print(f'{account}: {instance_count}')
    print('Total Instances Collected: ' + str(len(instance_id_list)))


def lambda_handler(event, context):
    logger.info("Event: %s", event)

    with open('non-reporting-instances.txt') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\n')
        line_count = 0
        for row in csv_reader:
            outdated_list.append(row[0])
        print("List: " + str(len(outdated_list)))

    grab_all_instances()

    # check if instance ids from outdated list exist
    for idx in outdated_list:
        try:
            id_index = instance_id_list.index(str(idx))
            valid_id_list.append({"InstanceId": instance_id_list[id_index], "CollectedIndex": id_index})
            # print('Index: ' + str(id_index))
        except ValueError as error:
            terminated_list.append(idx)
            # print(error)
        # else:
        # finally:

    print('Total Found Existing Instances: ' + str(len(valid_id_list)))

    # loop through outdated list
    for instance in outdated_list:
        try:
            # if not found in valid list, assumed terminated
            instance_found_index = instance_id_list.index(str(instance))

            # get from valid list
            for found in valid_id_list:
                if found['InstanceId'] == instance:
                    # print('Found Index: ' + str(found['CollectedIndex']))
                    json_data = instance_details_list[found['CollectedIndex']]
                    data = json.dumps(json_data, default=str)
                    data_dict = json.loads(data)
                    print("{} - {}".format(data_dict['InstanceId'], data_dict['State']['Name']))

        except ValueError as error:
            not_found_list.append(instance)

    # for instance in not_found_list:
    #     print("{} Not Found in List".format(instance))
    print("{} Not Found in List".format(len(not_found_list)))


if __name__ == "__main__":
    lambda_handler('', '')
