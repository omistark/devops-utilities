AWSTemplateFormatVersion: 2010-09-09
Description: This template will create and configure the resources needed for patching EC2 instances
Parameters:
  DevelopmentApproveAfterDays:
    Description: Patch Baseline for ITE Environment
    Type: Number
    Default: 1
  StagingApproveAfterDays:
    Description: Patch Baseline for QA Environment
    Type: Number
    Default: 7
  ProductionApproveAfterDays:
    Description: Patch Baseline for PROD Environment
    Type: Number
    Default: 14
  PatchingLogBucketName:
    Description: S3 Bucket for patching logs
    Type: String
    Default: "rc-admin-schwab-us-east-2"
Resources:
  # Patch Baselines
  LinuxITEBaseline:
    Type: AWS::SSM::PatchBaseline
    Properties:
      Name: RC-LinuxITEBaseline
      Description: Development patch baseline
      OperatingSystem: AMAZON_LINUX
      PatchGroups:
        - Development
      ApprovalRules:
        PatchRules:
          - PatchFilterGroup:
              PatchFilters:
                - Values:
                    - Critical
                    - Important
                    - Medium
                  Key: SEVERITY
                - Values:
                    - Security
                    - Bugfix
                  Key: CLASSIFICATION
                - Values:
                    - "*"
                  Key: PRODUCT
            ApproveAfterDays: !Ref DevelopmentApproveAfterDays
            ComplianceLevel: CRITICAL

  LinuxQABaseline:
    Type: AWS::SSM::PatchBaseline
    Properties:
      Name: RC-LinuxQABaseline
      Description: QA patch baseline
      OperatingSystem: AMAZON_LINUX
      PatchGroups:
        - Staging
      ApprovalRules:
        PatchRules:
          - PatchFilterGroup:
              PatchFilters:
                - Values:
                    - Critical
                    - Important
                    - Medium
                  Key: SEVERITY
                - Values:
                    - Security
                    - Bugfix
                  Key: CLASSIFICATION
                - Values:
                    - "*"
                  Key: PRODUCT
            ApproveAfterDays: !Ref StagingApproveAfterDays
            ComplianceLevel: CRITICAL

  LinuxPRODBaseline:
    Type: AWS::SSM::PatchBaseline
    Properties:
      Name: RC-LinuxPRODBaseline
      Description: Production patch baseline
      OperatingSystem: AMAZON_LINUX
      PatchGroups:
        - Production
      ApprovalRules:
        PatchRules:
          - PatchFilterGroup:
              PatchFilters:
                - Values:
                    - Critical
                    - Important
                    - Medium
                  Key: SEVERITY
                - Values:
                    - Security
                    - Bugfix
                  Key: CLASSIFICATION
                - Values:
                    - "*"
                  Key: PRODUCT
            ApproveAfterDays: !Ref ProductionApproveAfterDays
            ComplianceLevel: CRITICAL

  # Maintenace Window Configuration
  MaintenanceWindowDaily:
    Type: AWS::SSM::MaintenanceWindow
    Properties:
      AllowUnassociatedTargets: false
      Cutoff: 0
      Description: Maintenance window to run patching updates for ec2 instances
      Duration: 1
      Name: DevelopmentPatchingMaintenanceWindow
      Schedule: cron(0 00 02 ? * *)
      ScheduleTimezone: US/Eastern

  MaintenanceWindowWeekly:
    Type: AWS::SSM::MaintenanceWindow
    Properties:
      AllowUnassociatedTargets: false
      Cutoff: 0
      Description: Maintenance window to run patching updates for ec2 instances
      Duration: 1
      Name: StagingPatchingMaintenanceWindow
      Schedule: cron(0 00 02 ? * WED *)
      ScheduleTimezone: US/Eastern

  MaintenanceWindowBiweekly:
    Type: AWS::SSM::MaintenanceWindow
    Properties:
      AllowUnassociatedTargets: false
      Cutoff: 0
      Description: Maintenance window to run patching updates for ec2 instances
      Duration: 1
      Name: ProductionPatchingMaintenanceWindow
      Schedule: cron(0 00 02 ? * FRI#2 *)
      ScheduleTimezone: US/Eastern

  MaintenanceWindowTargetITE:
    Type: AWS::SSM::MaintenanceWindowTarget
    Properties:
      WindowId: !Ref "MaintenanceWindowDaily"
      ResourceType: INSTANCE
      Targets:
        - Key: tag:SecurityClassification
          Values:
            - ITE
      OwnerInformation: Daily Patching ITE
      Name: DailyPatchingITETarget
      Description: Patching targets for ITE
    DependsOn: MaintenanceWindowDaily

  MaintenanceWindowTargetQA:
    Type: AWS::SSM::MaintenanceWindowTarget
    Properties:
      WindowId: !Ref "MaintenanceWindowWeekly"
      ResourceType: INSTANCE
      Targets:
        - Key: tag:SecurityClassification
          Values:
            - QA
      OwnerInformation: Daily Patching QA
      Name: DailyPatchingQATarget
      Description: Patching targets for QA
    DependsOn: MaintenanceWindowWeekly

  MaintenanceWindowTargetPROD:
    Type: AWS::SSM::MaintenanceWindowTarget
    Properties:
      WindowId: !Ref "MaintenanceWindowBiweekly"
      ResourceType: INSTANCE
      Targets:
        - Key: tag:SecurityClassification
          Values:
            - PROD
      OwnerInformation: Daily Patching Production
      Name: DailyPatchingPRODTarget
      Description: Patching targets for Production
    DependsOn: MaintenanceWindowBiweekly

  MaintenanceWindowPatchingTaskITE:
    Type: AWS::SSM::MaintenanceWindowTask
    Properties:
      WindowId: !Ref "MaintenanceWindowDaily"
      Targets:
        - Key: WindowTargetIds
          Values:
            - !Ref "MaintenanceWindowTargetITE"
      TaskArn: AWS-RunPatchBaseline
      TaskType: RUN_COMMAND
      Priority: 1
      MaxConcurrency: 1
      MaxErrors: 1
      Name: DailyPatchingTask
      TaskInvocationParameters:
        MaintenanceWindowRunCommandParameters:
          CloudWatchOutputConfig:
            CloudWatchLogGroupName: !Ref PatchCWGroup
            CloudWatchOutputEnabled: true
          NotificationConfig:
            NotificationArn: !GetAtt PatchSNS.TopicArn
            NotificationEvents:
              - Failed
            NotificationType: Command
          OutputS3BucketName: !Ref PatchingLogBucketName
          OutputS3KeyPrefix: daily-patching-task
          Parameters: {
            "Operation":["Install"],
            "RebootOption": ["RebootIfNeeded"]
          }
          ServiceRoleArn: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM'
          TimeoutSeconds: 600
    DependsOn:
      - MaintenanceWindowTargetITE

  MaintenanceWindowPatchingTaskQA:
    Type: AWS::SSM::MaintenanceWindowTask
    Properties:
      WindowId: !Ref "MaintenanceWindowWeekly"
      Targets:
        - Key: WindowTargetIds
          Values:
            - !Ref "MaintenanceWindowTargetQA"
      TaskArn: AWS-RunPatchBaseline
      TaskType: RUN_COMMAND
      Priority: 1
      MaxConcurrency: 1
      MaxErrors: 1
      Name: DailyPatchingTask
      TaskInvocationParameters:
        MaintenanceWindowRunCommandParameters:
          OutputS3BucketName: !Ref PatchingLogBucketName
          OutputS3KeyPrefix: daily-patching-task
          Parameters: { "Operation": [ "Scan" ], "RebootOption": [ "NoReboot" ] }
          CloudWatchOutputConfig:
            CloudWatchLogGroupName: !Ref PatchCWGroup
            CloudWatchOutputEnabled: true
          NotificationConfig:
            NotificationArn: !GetAtt PatchSNS.TopicArn
            NotificationEvents:
              - Failed
            NotificationType: Command
          ServiceRoleArn: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM'
    DependsOn:
      - MaintenanceWindowTargetQA

  MaintenanceWindowPatchingTaskPROD:
    Type: AWS::SSM::MaintenanceWindowTask
    Properties:
      WindowId: !Ref "MaintenanceWindowBiweekly"
      Targets:
        - Key: WindowTargetIds
          Values:
            - !Ref "MaintenanceWindowTargetPROD"
      TaskArn: AWS-RunPatchBaseline
      TaskType: RUN_COMMAND
      Priority: 1
      MaxConcurrency: 1
      MaxErrors: 1
      Name: DailyPatchingTask
      TaskInvocationParameters:
        MaintenanceWindowRunCommandParameters:
          OutputS3BucketName: !Ref PatchingLogBucketName
          OutputS3KeyPrefix: daily-patching-task
          Parameters: { "Operation": [ "Scan" ], "RebootOption": [ "NoReboot" ] }
          CloudWatchOutputConfig:
            CloudWatchLogGroupName: !Ref PatchCWGroup
            CloudWatchOutputEnabled: true
          NotificationConfig:
            NotificationArn: !GetAtt PatchSNS.TopicArn
            NotificationEvents:
              - Failed
            NotificationType: Command
          ServiceRoleArn: !Sub 'arn:aws:iam::${AWS::AccountId}:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM'
    DependsOn:
      - MaintenanceWindowTargetPROD

  PatchSNS:
    Type: AWS::SNS::Topic
    Properties:
#      ContentBasedDeduplication: Boolean
#      DataProtectionPolicy: Json
      DisplayName: ec2-patching
#      FifoTopic: Boolean
#      KmsMasterKeyId: String
#      SignatureVersion: String
#      Subscription:
#        - Endpoint: String
#          Protocol: Email
      Tags:
        - Key: SecurityClassification
          Value: PROD
      TopicName: EC2-Patching
#      TracingConfig: 'Active'

  PatchCWGroup:
    Type: AWS::Logs::LogGroup
    Properties:
#      DataProtectionPolicy: Json
#      KmsKeyId: String
      LogGroupName: /aws/ssm/daily-patching-task
      RetentionInDays: 365
      Tags:
        - Key: SecurityClassification
          Value: PROD

