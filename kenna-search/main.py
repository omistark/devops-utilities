# This is a sample Python script.
import json
import logging
import boto3

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from botocore.exceptions import ClientError

accounts = [
    {'acct_no': '791906096096', 'profile_name': 'rc-apex-RiskCanvasAdministrators'},
    {'acct_no': '087748086814', 'profile_name': 'rc-bts-RiskCanvasAdministrators'},
    {'acct_no': '729587188256', 'profile_name': 'rc-core-RiskCanvasAdministrators'},
    {'acct_no': '091680774299', 'profile_name': 'rc-shared-RiskCanvasAdministrators'},
    {'acct_no': '977870926058', 'profile_name': 'rc-tda-RiskCanvasAdministrators'},
    {'acct_no': '157161670134', 'profile_name': 'rc-vanguard-RiskcanvasAdministrators'},
    {'acct_no': '830569890982', 'profile_name': 'rc-westpac-RiskcanvasAdministrators'}
]
regions = ['us-east-2',
           'us-east-1', 'us-west-1', 'us-west-2',
           'ap-east-1', 'ap-south-1',
           'ap-northeast-3', 'ap-northeast-2', 'ap-southeast-1', 'ap-southeast-2', 'ap-northeast-1',
           'ca-central-1', 'eu-central-1', 'eu-west-1', 'eu-west-2', 'eu-south-1', 'eu-west-3', 'eu-north-1',
           'me-south-1', 'sa-east-1', 'af-south-1']
# postgres
# assets = ['10.1.3.66', '10.1.3.178', '10.2.3.233', '10.1.3.44', '10.1.3.248', '10.1.3.115', '10.1.3.24',
#           '10.1.3.242', '10.1.3.75', '10.1.3.55', '10.1.3.155', '10.1.3.7', '10.1.3.25', '10.1.3.36',
#           '10.1.3.85', '10.1.3.92', '10.2.3.248', '10.2.3.68', '10.2.3.252', '10.2.3.181', '10.2.3.54',
#           '10.2.3.183', '10.2.3.4', '10.2.3.62', '10.2.3.101', '10.2.3.72', '10.2.3.152', '10.1.3.254',
#           '10.1.3.108', '10.1.3.32', '10.3.3.133', '10.3.3.80', '10.3.3.222', '10.3.3.116', '10.1.3.19',
#           '10.3.3.138', '10.1.3.102', '10.1.3.136', '10.3.3.214', '10.3.3.71', '10.3.3.228', '10.3.3.11',
#           '10.4.3.205', '10.4.3.125', '10.4.3.4', '10.4.3.161', '10.4.3.196', '10.2.3.17']
# sudo
# assets = ['10.1.3.44', '10.1.3.248', '10.1.3.115', '10.1.3.24', '10.2.3.17']


# sudo
# assets = ['ip-10-1-3-170.ec2.internal', 'ip-10-1-3-110.ec2.internal', 'ip-10-0-1-149.ec2.internal',
#           'ip-10-1-3-63.ec2.internal', 'ip-10-1-3-137.ec2.internal']
assets = ['10.2.3.147']


def print_hi(name):
    for account in accounts:
        for region in regions:
            session = boto3.session.Session(region_name=region, profile_name=account['profile_name'])
            ec2_resource = session.resource('ec2')
            try:
                for instance in ec2_resource.instances.all():
                    for asset in assets:
                        if instance.private_ip_address == asset:
                            # if instance.private_dns_name == asset:
                            # print(instance.tags)
                            name = ''
                            classification = 'none'
                            for tag in instance.tags:

                                if tag['Key'] == 'Name':
                                    name = tag['Value']
                                if tag['Key'] == 'SecurityClassification':
                                    classification = tag['Value']
                            print(
                                "Instance ID: " + instance.id + ", IP Address: " + instance.private_ip_address +
                                ", Profile: " + account['profile_name'] +
                                ", Name: " + name + ", Classification: " + classification
                                # , '\n'
                            )
                    # Use a breakpoint in the code line below to debug your script.
                    # print(f"Hi, {account['acct_no']}")  # Press ⌘F8 to toggle the breakpoint.
            except ClientError as e:
                x = e
                # print("Error", account, region, e)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
