#!/bin/bash
export AWS_PROFILE=rc-account-RiskcanvasAdministrators
export AWS_REGION=us-east-1
GROUP_ID=sg-0123456789

# run tests
set -eo pipefail

aws ec2 describe-network-interfaces --filters Name=group-id,Values=$GROUP_ID --region $AWS_REGION --output json