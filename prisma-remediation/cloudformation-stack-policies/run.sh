#!/bin/bash
export AWS_PROFILE=rc-vanguard-RiskcanvasAdministrators
export AWS_REGION=us-east-1
STACK_NAME=screening-service-disposition-callback-uat

# run tests
set -eo pipefail

aws cloudformation set-stack-policy --stack-name $STACK_NAME --stack-policy-body file://stack-policy.json --region $AWS_REGION