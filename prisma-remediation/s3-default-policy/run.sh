#!/bin/bash
export AWS_PROFILE=rc-account-RiskcanvasAdministrators
export AWS_REGION=us-east-1
BUCKET_NAME=my-bucket-name

# run tests
set -eo pipefail

aws s3api put-bucket-policy --bucket $BUCKET_NAME --policy file://default-policy.json