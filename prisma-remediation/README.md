# PRISMA REMEDIATION #

**cloudformation-stack-policies** - Contains a shell script and policy JSON file. Edit the file 
'stack-policy.json' with your desired permissions. Then edit the shell script to set the STACK_NAME
variable to the CloudFormation stack name

**default-security-group-search** - Contains a shell script. Set the GROUP_ID variable to the
security group id to search which AWS resources are using the security group 

**s3-default-policy** - Contains a shell script and policy JSON file. Edit the file 'default-policy.json'
with the Resource attribute with the desired S3 bucket name. Then edit the shell script to set the
BUCKET_NAME variable with the S3 bucket name