# DevOps Utilities


**AWS Config Recording** - Cloudformation template to create alarm for configuration changes to 'PROD'
AWS resources

**Infosec Playbook** - Ansible playbook for latest os package updates

**Infra Terraform** - Terraform scripts for environment build

**Kenna Search** - search AWS accounts by private IP address

**Prisma Remediation** - scripts to update AWS resources

**Transactions Summaries** - runs queries through AWS Athena to retrieve transactions, check Elasticsearch
for a match, and create report.

**Screening File Monitor** - captures AWS S3 "PUT" action occurs then run an S3 call to check for that S3 
object successful.

**Sanctions Screening Monitor** - scans trace summaries from AWS X-Ray looking for instances of transaction
IDs, queries Elasticsearch for item ids, and create report.