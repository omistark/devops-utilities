import json
import ast
import boto3
import os

session = boto3.session.Session(region_name=os.environ['AWS_REGION'])
sns_client = session.client('sns')


def lambda_handler(event, context):
    convert_event = str(event).replace("'", '"')
    loaded_json = json.loads(str(convert_event))

    # TODO implement
    print(loaded_json['detail']['configurationItem']['tags']['SecurityClassification'])

    if loaded_json['detail']['configurationItem']['tags']['SecurityClassification'] == 'PROD':
        response = sns_client.publish(
            TopicArn='arn:aws:sns:us-east-1:157161670134:Admin-Prod-Config',
            Message=str(loaded_json),
            Subject='Production Event Change'
        )
        print(response)

    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
