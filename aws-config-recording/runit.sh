#!/bin/bash
#export AWS_PROFILE=rc-tda-RiskCanvasAdministrators
#export AWS_REGION=us-east-2

set -eo pipefail

##### deploy function #####
aws --region $AWS_REGION cloudformation deploy --template-file cwconfig.yml --stack-name Admin-Stack --capabilities CAPABILITY_NAMED_IAM
aws --region $AWS_REGION cloudformation set-stack-policy --stack-name Admin-Stack --stack-policy-body file://default-stack-policy.json