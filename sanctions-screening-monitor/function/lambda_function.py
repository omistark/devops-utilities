import json
import logging
import boto3
import requests
import warnings
import csv
import os
import time
import base64
from botocore.exceptions import ClientError
from datetime import datetime, timedelta
from jsonpath_ng import parse
from pytz import timezone
from decimal import Decimal

warnings.filterwarnings('ignore', message='Unverified HTTPS request')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

session = boto3.session.Session(region_name=os.environ['AWS_REGION'])
xray_client = session.client('xray')
s3_client = session.client('s3')
# secrets_client = session.client('secretsmanager')

total_requests = 0
holds = 0
releases = 0
errors = 0
total_record_count = 0
total_release_count = 0
total_error_count = 0
total_hold_count = 0
annotations = []
tracking_ids = []
matched_list = []
unmatched_list = []
request_records = []
transaction_records = []
match_csv_file_mode = 'w'
unmatch_csv_file_mode = 'w'
request_csv_file_mode = 'w'
transaction_csv_file_mode = 'w'
summary_csv_file_mode = 'w'
fmt = "%Y-%m-%dT%H:%M:%S%z"

request_fields = ['record_count', 'release_count', 'error_count', 'hold_count', 'has_error', ]
request_r2_fields = ['start_time', 'id']
transaction_fields = ['start_time', 'end_time', 'id']
request_headers = ['lookup_id', 'record_count', 'release_count', 'error_count', 'hold_count', 'has_error', 'start_time',
                   'id', 'resource_names']

s3_bucket = os.environ['CSV_S3_BUCKET']
s3_path = str(datetime.today().date())
query_day = ''
trace_retrival_interval = -30
match_headers = ['itemId', 'id', 'table_name', 'tracking_id']

VANGUARD_URL = 'https://riskcanvas.vanguard.com/eds/items/find'
headers = {
    'ticket': 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLnJ6VVlXbDhGamdFRkpGS3pTY1ZFc3cubVFBc0FkbVh6NWtjWVkxNTNyVkdCU1U3NDdFREJieEN4SzRZYjhpNnlKY1NWb3NHS3VoQV94WE53MElEMXNSRmpOWnJtYURlR245NzE0dENJcVduRGM0c2JsMkFVcVZzUjNWWEt0eTZvNWpibHl5b2lIMWI5SDZNZ2pZdVRMRHBua2NzOWhRZzVOYXBvUjJ2TjdFOUtET3pqZVRaNnpZMG5LY1dLd3Vsb3IzMHpDZS1leGFJSFhUVlE0TWpmVVpjS2pJZHpLdldNeElvRERFd3o4RkkwZVZUTXM1SzJ2R285c0RwUC1KaC1tRzFyWmNyWWJLS2tTdjZ1bnpJb1I5TDNXRTFLZVNrZnk0d0ROZG1OWEJRYmNwQktoU3pud2RBS2RXOVc2NjZRY1ROLWpzMk8ydzVZZzZtZTdMZkNnT3FoeGhSZWdlZS1SZEg1TExVandJYjdrZTdwQXRLYUpNTVRTSDZDV0luVmpKcnJZSDZCdHhBTjUtX1dBSC1vX0huVVlXN3BUd3FzUG05VkdiZ2JZX29LUnlQZE5lNTVnSElfMkc0ZWdsT2dUMHJ3TVhzSGczcnJKU0hHeVl0dWlPUXlvUnI0QnY0WFd2ZS1idndVaDVDTXdvWXpyb01RNHU4WXFfbXNWcXlkTm43dGNMUVY5Y2ZMVzE0UERqbUR1cnguMl8zY2tPX0ZoZ1RLQVozdFZzQzBHQQ.KUJao3U10HH-WiyXNpgt6vDqsGoyj1lBl3WErT2dFWcyCG-xVa65cN0qukyRncHvBFm-XY2MPPcjE73YOoeoLA',
    'Content-Type': 'application/json'
}


def lambda_handler(event, context):
    # global headers
    # headers['ticket'] = get_secret()
    # logger.info('## Headers: ' + str(headers))
    scrub_sanctions_logs(event['time'])
    result_message = {
        'matches': matched_list,
        'message': 'We are able pull counts from sanctions screening!'
    }
    return {
        'statusCode': 200,
        'body': json.dumps(result_message)
    }


def test_handler(event, context):
    global session, xray_client, s3_client, secrets_client
    session = boto3.session.Session(region_name=os.environ['AWS_REGION'], profile_name=os.environ['AWS_PROFILE'])
    xray_client = session.client('xray')
    s3_client = session.client('s3')
    secrets_client = session.client('secretsmanager')

    headers['ticket'] = get_secret()
    logger.debug('## Headers: ' + str(headers))
    return lambda_handler(event, context)


def scrub_sanctions_logs(event_time):
    global tracking_ids, query_day, s3_path, xray_client, s3_client
    query_end_time = datetime.strptime(event_time, '%Y-%m-%dT%H:%M:%SZ')
    query_start_time = query_end_time + timedelta(minutes=trace_retrival_interval)
    query_day = str(query_end_time.date())
    s3_path = str(query_day)

    # append
    pull_file_from_s3()

    # pull requests from x-ray logs
    # XRay only lets us query a range up to 24 hours, so break down and aggregate
    logger.info('## Query Day: ' + str(query_day))
    logger.info('## Query Start Time: ' + str(query_start_time))
    logger.info('## Query End Time: ' + str(query_end_time))
    query_xray(query_start_time, query_end_time)

    # create summary csv
    logger.info('## Total Requests: ' + str(total_requests))
    logger.info('## Total Holds: ' + str(holds))
    logger.info('## Total Releases: ' + str(releases))
    logger.info('## Total Errors: ' + str(errors))

    # # output into csv to S3
    write_tracking_to_csv()
    write_summaries_to_csv()

    # individual csv per day (24 hours)
    push_to_s3()


def query_xray(start_time, end_time):
    next_token = '-1'

    xray_response = xray_client.get_trace_summaries(
        StartTime=start_time,
        EndTime=end_time,
        TimeRangeType='Event',
        FilterExpression='annotation.service = \"Screening\"'
    )
    process_xray_response(xray_response)
    if "NextToken" in xray_response:
        next_token = xray_response['NextToken']

    while next_token != '-1':
        xray_response = xray_client.get_trace_summaries(
            StartTime=start_time,
            EndTime=end_time,
            TimeRangeType='Event',
            FilterExpression='annotation.service = \"Screening\"',
            NextToken=next_token
        )

        process_xray_response(xray_response)

        if "NextToken" in xray_response:
            next_token = xray_response['NextToken']
        else:
            next_token = '-1'
    logger.info('## Total Ids: ' + str(len(transaction_records)))


def process_xray_response(xray_response):
    global total_requests, holds, releases, errors, tracking_ids
    trace_ids = []
    # get_request_info(xrayResponse)
    if "TraceSummaries" in xray_response:
        for trace in xray_response['TraceSummaries']:
            total_requests = total_requests + trace['Annotations']['record_count'][0]['AnnotationValue']['NumberValue']
            holds = holds + trace['Annotations']['hold_count'][0]['AnnotationValue']['NumberValue']
            releases = releases + trace['Annotations']['release_count'][0]['AnnotationValue']['NumberValue']
            errors = errors + trace['Annotations']['error_count'][0]['AnnotationValue']['NumberValue']
            logger.debug('## Trace Details: ' + str(trace))

            # filter trace ids
            trace_ids.append(trace['Id'])

        # get trace details
        for trace_id in trace_ids:
            response = xray_client.batch_get_traces(TraceIds=[trace_id])
            for trace in response['Traces']:
                get_request_info(trace)


def search_match(tracking):
    logger.debug('## ElasticSearch Searching for: ' + str(tracking) + ' at ' + str(VANGUARD_URL))
    item_id = 'none'
    payload = {"query": {"terms": {"tracking_id": [tracking]}}, "_source": ["itemId", "tracking_id"]}
    logger.debug('## ElasticSearch Headers: ' + str(headers))
    logger.debug('## ElasticSearch Payload: ' + str(payload))

    try:
        response = requests.post(VANGUARD_URL, data=json.dumps(payload),
                                 headers=headers, verify=False)
        logger.debug('## ElasticSearch Response: ' + str(response.text))
        search_resp = json.loads(response.text)
        logger.debug('## ElasticSearch Code: ' + str(search_resp['code']))

        if search_resp['code'] == '200':
            # Add to match list
            if search_resp['totalHits'] > 0:
                logger.info('## Elasticsearch Match Successful: ' + str(search_resp['data']))
                matched_list.append(search_resp['data'][0])
                es_response_data = search_resp['data'][0]
                item_id = es_response_data['itemId']
            # Add to unmatched list
            else:
                unmatched_list.append(tracking)
        else:
            item_id = search_resp['code']
            logger.error('## ElasticSearch Response: ' + str(response.text))
    except requests.exceptions.ConnectionError as e:
        logging.error(e)

    return item_id


def write_tracking_to_csv():
    global request_csv_file_mode, transaction_csv_file_mode
    logger.debug('## Request File Mode: ' + str(request_csv_file_mode))
    with open('/tmp/' + query_day + '.requests.csv', request_csv_file_mode, newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # If new file
        if request_csv_file_mode == 'w':
            spamwriter.writerow(request_headers)

        for request_record in request_records:
            start_time_conv = convert_time_secs_to_est(request_record['start_time'])
            x = (request_record['lookup_id'], request_record['record_count'], request_record['release_count'],
                 request_record['error_count'],
                 request_record['hold_count'], request_record['has_error'], start_time_conv,
                 request_record['id'], request_record['resource_names'])

            logger.debug('## Writing Request Record: ' + str(x))
            spamwriter.writerow(x)

    f = open('/tmp/' + query_day + '.requests.csv', 'r')
    logger.debug(f.read())
    f.close()

    logger.debug('## Transaction File Mode: ' + str(transaction_csv_file_mode))
    with open('/tmp/' + query_day + '.transactions.csv', transaction_csv_file_mode) as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # If new file
        if transaction_csv_file_mode == 'w':
            spamwriter.writerow(['tracking_id', 'start_time', 'end_time', 'id', 'lookup_id', 'item_id'])
        for transaction_record in transaction_records:
            start_time_conv = convert_time_secs_to_est(transaction_record['start_time'])
            end_time_conv = convert_time_secs_to_est(transaction_record['end_time'])
            x = (transaction_record['tracking_id'], start_time_conv, end_time_conv, transaction_record['id'],
                 transaction_record['lookup_id'], transaction_record['item_id'])
            logger.debug('## Writing Transaction: ' + str(x))
            spamwriter.writerow(x)

    f = open('/tmp/' + query_day + '.transactions.csv', 'r')
    logger.debug(f.read())
    f.close()


def write_summaries_to_csv():
    global total_record_count, total_release_count, total_hold_count, total_error_count
    t_file = open('/tmp/' + query_day + '.transactions.csv')
    r_file = open('/tmp/' + query_day + '.requests.csv')
    t_reader = csv.reader(t_file)
    t_lines = len(list(t_reader))
    r_reader = csv.reader(r_file)
    r_lines = len(list(r_reader))
    t_file.close()
    r_file.close()

    with open('/tmp/' + query_day + '.requests.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i = 0
        for request_record in spamreader:
            if i > 0:
                print('R: ' + str(request_record[1]))
                total_record_count += Decimal(request_record[1])
                total_release_count += Decimal(request_record[2])
                total_error_count += Decimal(request_record[3])
                total_hold_count += Decimal(request_record[4])
            i += 1

    logger.debug('## Summaries File Mode: ' + str(summary_csv_file_mode))
    with open('/tmp/' + query_day + '.summaries.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # If new file
        if summary_csv_file_mode == 'w':
            spamwriter.writerow(['total_transactions', 'total_requests', 'total_record_count', 'total_release_count',
                                 'total_error_count', 'total_hold_count'])

        x = [t_lines, r_lines, total_record_count, total_release_count, total_error_count, total_hold_count]
        logger.debug('## Writing Transaction: ' + str(x))
        spamwriter.writerow(x)

    f = open('/tmp/' + query_day + '.summaries.csv', 'r')
    print(f.read())
    f.close()


def pull_file_from_s3():
    global request_csv_file_mode, transaction_csv_file_mode, s3_path, query_day
    try:
        logger.info('## Pulling Requests CSV File: ' + s3_path + '/' + query_day + '.requests.csv')
        s3_client.download_file(s3_bucket, s3_path + '/' + query_day + '.requests.csv',
                                '/tmp/' + query_day + '.requests.csv')
        logger.info('## Requests File Retrieval Successful')
        request_csv_file_mode = 'a+'
    except ClientError as e:
        logging.error(e)

    try:
        logger.info('## Pulling Transactions CSV File: ' + s3_path + '/' + query_day + '.transactions.csv')
        s3_client.download_file(s3_bucket, s3_path + '/' + query_day + '.transactions.csv',
                                '/tmp/' + query_day + '.transactions.csv')
        logger.info('## Transactions File Retrieval Successful')
        transaction_csv_file_mode = 'a+'
    except ClientError as e:
        logging.error(e)

    try:
        logger.info('## Pulling CSV File: ' + s3_path + '/' + query_day + '.summaries.csv')
        s3_client.download_file(s3_bucket, s3_path + '/' + query_day + '.summaries.csv',
                                '/tmp/' + query_day + '.summaries.csv')
        logger.info('## Summaries File Retrieval Successful')
        # summaries_csv_file_mode = 'a+'
    except ClientError as e:
        logging.error(e)


def push_to_s3():
    global s3_path, query_day
    logger.info('## S3 Path: ' + str(s3_path))
    try:
        logger.info('## Saving Requests CSV file: ' + str(s3_path + '/' + query_day + '.requests.csv'))
        response = s3_client.upload_file('/tmp/' + query_day + '.requests.csv', s3_bucket,
                                         s3_path + '/' + query_day + '.requests.csv')
        logger.info('## Saving Transactions CSV file: ' + str(s3_path + '/' + query_day + '.transactions.csv'))
        response = s3_client.upload_file('/tmp/' + query_day + '.transactions.csv', s3_bucket,
                                         s3_path + '/' + query_day + '.transactions.csv')
        logger.info('## Saving Summaries CSV file: ' + str(s3_path + '/' + query_day + '.summaries.csv'))
        response = s3_client.upload_file('/tmp/' + query_day + '.summaries.csv', s3_bucket,
                                         s3_path + '/' + query_day + '.summaries.csv')
    except ClientError as e:
        logging.error(e)


def get_request_info(data):
    data = json.dumps(data)
    data = json.loads(data)

    for segment in data['Segments']:
        s = segment['Document'].replace("'", "\"")
        s = json.loads(s)
        logger.debug('## Document Id: ' + str(s['id']))
        request_record = {'lookup_id': data['Segments'][0]['Id'], 'segment_id': s['id']}
        jsonpath_expr = parse('$..record_count')
        for match in jsonpath_expr.find(s):
            for field in request_fields:
                request_record[field] = match.context.value[field]
            for field in request_r2_fields:
                request_record[field] = match.context.context.value[field]

        jsonpath_expr = parse('$..resource_names')
        request_record['resource_names'] = []
        for match in jsonpath_expr.find(s):
            request_record['resource_names'].append(match.value)

        if 'record_count' in request_record.keys():
            request_records.append(request_record)
            logger.debug('## Request Record: ' + str(request_record))

        jsonpath_expr = parse('$..tracking_id')
        for match in jsonpath_expr.find(s):
            record = {'tracking_id': match.value}
            for field in transaction_fields:
                record[field] = match.context.context.context.value[field]
            record['item_id'] = search_match(record['tracking_id'])
            record['lookup_id'] = data['Segments'][0]['Id']
            logger.debug('## Transaction Record: ' + str(record))
            transaction_records.append(record)
            continue


def get_secret():
    secret_name = os.environ['ES_API_SECRET']
    logger.info('## Secret Name: ' + str(secret_name))
    try:
        get_secret_value_response = secrets_client.get_secret_value(
            SecretId=secret_name
        )
        logger.info('## Secret Response: ' + str(get_secret_value_response))
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            # logging.error(e)
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            # logging.error(e)
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            # logging.error(e)
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            # logging.error(e)
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            #
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            val = json.loads(secret)
            logger.info('## Secret Response: ' + str(val))
            return val['ticket']
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            val = json.loads(decoded_binary_secret)
            logger.info('## Decoded Secret: ' + str(val))
            return val['ticket']


def convert_time_secs_to_est(utc_seconds):
    date_str = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(utc_seconds))
    datetime_obj = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    datetime_obj_utc = datetime_obj.replace(tzinfo=timezone('UTC'))
    logger.debug('## UTC Time: ' + datetime_obj_utc.strftime("%Y-%m-%d %H:%M:%S %Z%z"))

    time_est = datetime_obj_utc.astimezone(timezone('US/Eastern'))
    time_est_fmtted = time_est.strftime(fmt)
    logger.debug('## EST Time Conversion: ' + str(time_est_fmtted))
    return time_est_fmtted

# if __name__ == "__main__":
#     print('Formatted: ' + convert_time_secs_to_est(1616605037.973469))
