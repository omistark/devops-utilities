import unittest
import importlib
import logging
import jsonpickle
import json
import boto3
from botocore.config import Config

# from aws_xray_sdk.core import xray_recorder

logger = logging.getLogger()
# xray_recorder.configure(
#   context_missing='LOG_ERROR'
# )
# function = importlib.import_module(lambda_function)

# xray_recorder.begin_segment('test_init')
function = __import__('lambda_function')
handler = function.test_handler


# xray_recorder.end_segment()


class TestFunction(unittest.TestCase):

    def test_function(self):
        # xray_recorder.begin_segment('test_function')
        file = open('event.json', 'rb')
        try:
            ba = bytearray(file.read())
            event = jsonpickle.decode(ba)
            logger.warning('## EVENT')
            logger.warning(jsonpickle.encode(event))
            context = {'requestid': '1234'}
            result = handler(event, context)
            print(str(result['body']))
            x = json.loads(result['body'])
            print('Body: ' + str(x['message']))
            self.assertRegex(str(x['message']), 'We are able pull counts from sanctions screening!',
                             'Should match')
        finally:
            file.close()
        file.close()
        # xray_recorder.end_segment()


if __name__ == '__main__':
    unittest.main()
