# SANCTIONS SCREENING MONITOR #



### What is this repository for? ###

This is a Python script designed to be uploaded to AWS Lambda as a function
and scans trace summaries from AWS X-Ray looking for instances of transaction
IDs. Once it collects these IDs, it queries them against an Elasticsearch endpoint
looking for correlating Item Ids. Finally, it condenses the records into CSV files 
and stores them in an AWS S3 bucket.

### How do I get set up? ###

* Summary of set up
  
* Configuration
- S3 bucket
- Lambda XRay

* Dependencies
1. S3 bucket (To pull CSV files. The bucket should already be created)
2. XRay trace (To pull execution summary details. This is referring to the Sanctions
   Lambda Function being setup)
3. Elasticsearch server (To pull tracking ids from processed records. It can be 
   an endpoint or IP address) 
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact