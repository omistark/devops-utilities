# Remediation

## Prisma 
The alert homepage gives you a list of alerts that require configuration changes the need 
to be fixed. (1) Gives filters type to pick out alerts by account or alert. (2) Select which 
accounts to view alerts for. (3) The severity level for an alert
![Server Map](images/prisma-alert-home.png)

The detail page has the information necessary to include in the JIRA tickets created for fixing
an alert and help to fix the alert. (1) The policy information describing why and how the alert
creates a security risk. (2) The step-by-step process to use in order to fix the alert. (3) 
Information regarding the individual AWS resource alerts including the Prisma ID, the name in AWS,
the AWS account it belongs to, the region, and more.

![Server Map](images/prisma-alert-details.png)

## Kenna

The Kenna homepage has overall statistics for instances owned by Risk Canvas. Navigate to the dashboard
section (1). "Cloud_AWS_RiskCanvas" is the dashboard to select for information regarding the client accounts.
(2) The Top Fixes button will take you a detail page listing the most critical fixes. On the detail page, you 
will be able to select from the "Top Fix Group" selector (3). Each option shows how much the score will go 
down once a fix has been taken care of. The information will then load below it. (4) The target platform, package
name, and Security ID reference will be shown. Below that, there will more information under tabs. Under the "Solution"
tab (5), this will contain instructions on the steps to update/configure the selected package. Under the 
"Assets Affected", there will be a list of IP addresses for the instances alerted. 
![Server Map](images/kenna-home.png)
![Server Map](images/kenna-dashboards.png)
![Server Map](images/kenna-details.png)