import json
import logging
import urllib3
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

hook_url = os.environ["SLACK_WEBHOOK_URL"]


def lambda_handler(event, context):
    logger.info("Event: %s", event)
    msg = json.loads(event['Records'][0]['Sns']['Message'])

    if 'PROD' in msg['AlarmName']:
        channel = '#emergency-devops-alerts'
    else:
        channel = '#nonprod-devops'

    slack_message = {
        'channel': channel,
        # 'text': 'test',
        'text': str("Account Name: {{customer_name}} / Alarm Name: " + msg['AlarmName'] +
                    " / Alarm Description: " + msg['AlarmDescription']),
        'icon_emoji': ":anger:"
    }
    http = urllib3.PoolManager()
    encoded_msg = json.dumps(slack_message).encode('utf-8')
    response = http.request('POST', hook_url, body=encoded_msg)
    result = response.status
    logger.info("Response Status: %s", response.status)
    logger.info("Response Data: %s", response.data)
    logger.info("Message posted to %s", slack_message['channel'])
    response = {'result': result}
    return response


if __name__ == "__main__":
    lambda_handler('', '')
