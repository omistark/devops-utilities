# Monitoring Server

This playbook is setup 

```
ansible-playbook scoringcluster.yml -i hosts --extra-vars "@client_vars_file.json"
```


The host file need to declare the Application instances to connect to. It needs to connect to the Application instances
to install the Amazon Cloudwatch Agent. 


Variables you can include in var file:
```
slack_webhook:
slack_channel:
target_environment
security_classification

agent_config_path
log_group_name
kms_key

slack_sns_arn
cw_lambda_name
cw_lambda_arn
emr.id
<service>.lb_id
<service>.tg_id

alarm_builtin_list:
- { name: ServerLife-EMR, metric_namespace: AWS/ElasticMapReduce, metric_name: MRLostNodes, alarm_condition: 'GreaterThanThreshold', alarm_threshold: 0.0, alarm_dimension: { JobFlowId: "{{emr.id}}" }, alarm_description: "This will alarm when a EMR node is encountering an Error"}
- { name: ServerLife-Elastic, metric_namespace: AWS/NetworkELB, metric_name: HealthyHostCount, alarm_condition: 'LessThanThreshold', alarm_threshold: 1.0, alarm_dimension: {LoadBalancer: "{{elastic.lb_id}}", TargetGroup: "{{elastic.tg_id}}"}, alarm_description: "This will alarm when an Elasticsearch node is not registering as online"}
```

Log into console and manually add trigger to Lambda function

## Alarm Sets
### Target Group Metric Alarms
- Requires Load Balancer and Target Group
### Disk Space Metric Alarms
- Requires installation of Amazon Cloudwatch Agent on EC2 and agent configuration file, specifically the metrics section
### Application Log Group Alarms
- Requires installation of Amazon Cloudwatch Agent on EC2 and agent configuration file, specifically the logs section
### EMR
- Requires EMR Id
